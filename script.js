﻿$(document).ready(function() {
	     var tabeliridu = $("#korvitabel tr").length;
	     var peidustabeliridu = 0;
	
	
	//-------------------toote suurpildi kuvamine-----------------------------------
	     
         var src2 = ""; // algv22rtus
         var mitutoodetkorvis = 0;
         
         $(".tootepeapilt1, .tootekorvalpildid").on({
                 mouseenter: function(){ 
	                        src2 = $(this).attr("src"); 
                            src2 = src2.replace("thumb", "img");
                            $(".suurpilt").css("display", "block");
                            $(".suurpilt").find("img").attr("src", src2);
                            },
                 mouseleave: function(){ 
	                        $(".suurpilt").css("display", "none"); 
							},
		}); 
		
	//-------------------ostukorvi lisamine - ajax-----------------------------------	
		
		var tooteid = "";
		
		function animekorvitagasiside(){
	           $("#teade").css("display", "block").delay( 800 );
               $("#teade").fadeOut(500).css("display", "block");      
        };
         
		
		$(".lisaostukorvi5, .lisaostukorvi2, .lisaostukorvi1").on({
                 click: function(){ 
	                        tooteid = $(this).attr("data-row");
	                        $.ajax({url: "?mode=lisaostukorvi", data: "id=" + tooteid});
	                        animekorvitagasiside();
                            },
		}); 
		
		
		//-------------------ostukorvi info uuendaja - ajax-----------------------------------
		
		function lisatyhikudtuhandikele(number){
			   number = number + "";
			   numbriosad = number.split(".");
			   massiiv = numbriosad[0].split("").reverse(); // tuhandike lugemist alustatakse tagant
			   
			   loppnumber = "";
			   
			   for (i = 0; i < massiiv.length; i++) {
				   if(i%3 == 0 && i != 0){
					   loppnumber += " ";
				   }  
				   loppnumber += massiiv[i];
			   }
			   loppnumber = loppnumber.split("").reverse().join("");
			   loppnumber = loppnumber + "," + numbriosad[1]; // lisab komakohale koma
 
	           return loppnumber;   
        };
		
		$(".paneuksjuurde").on({
                 click: function(){ 
	                        tooteid = $(this).attr("data-row");
	                        if ( $( "#tellimuseinfo2" ).length ) {
                               tellimusenr = $("#tellimusekustutaja").attr("data-row");
                               $.ajax({url: "?mode=lisatellimusse", data: {id: tellimusenr, id2: tooteid}});
                            } else {
                            
	                        $.ajax({url: "?mode=lisaostukorvi", data: "id=" + tooteid});
	                        }
	                        tk = $(this).parent().parent().children(".mitutk").html();
	                        tk++;
	                        $(this).parent().parent().children(".mitutk").html(tk);
	                        
	                        hindraw = $(this).parent().parent().parent().find("td:nth-child(3)").html();
	                        hindraw = hindraw.replace(" ", "");
	                        hindinfo = hindraw.split(',');
	                        hindtaisarv = hindinfo[0];
	                        hindsendid = parseInt(hindinfo[1]);
	                        hind = parseFloat(hindtaisarv + "." +  hindsendid);
	                        
	                        alasummaraw = $(this).parent().parent().parent().find("td:nth-child(5)").html();
	                        alasummaraw = alasummaraw.replace(" ", "");
	                        alasummainfo = alasummaraw.split(',');
	                        alasummataisarv = parseInt(alasummainfo[0]);
	                        alasummasendid = parseFloat(alasummainfo[1]);
	                        alasummavana = parseFloat(alasummataisarv + "." +  alasummasendid);
	                        
	                        loppsummaraw = $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html();
	                        loppsummaraw = loppsummaraw.replace(" ", "");
	                        loppsummainfo = loppsummaraw.split(',');
	                        loppsummataisarv = loppsummainfo[0];
	                        loppsummasendid = parseInt(loppsummainfo[1]);
	                        loppsumma = parseFloat(loppsummataisarv + "." +  loppsummasendid);
	                        
	                        loppsumma = loppsumma - alasummavana;
	                     
	                        alasumma = hind * tk;
	                        
	                        $(this).parent().parent().parent().find("td:nth-child(5)").html(lisatyhikudtuhandikele(alasumma.toFixed(2)) + "€");
	                        lopphind = loppsumma + alasumma;
	                        $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html(lisatyhikudtuhandikele(lopphind.toFixed(2)) + "€");
                        
                            },
		}); 
		
		$(".votauksvahemaks").on({
                 click: function(){ 
	                        tooteid = $(this).attr("data-row");
	                        
	                        if ( $( "#tellimuseinfo2" ).length ) {
                               tellimusenr = $("#tellimusekustutaja").attr("data-row");
                               $.ajax({url: "?mode=vahandatellimusest", data: {id: tellimusenr, id2: tooteid}});
                            } else {
	                         $.ajax({url: "?mode=vahandaostukorvist", data: "id=" + tooteid});
	                        }
	                        
	                        tk = $(this).parent().parent().children(".mitutk").html();
	                        if(tk != 1){
		                       tk--;   
	                        }
	                        
	                        
	                        $(this).parent().parent().children(".mitutk").html(tk);
                            
                            hindraw = $(this).parent().parent().parent().find("td:nth-child(3)").html();
	                        hindraw = hindraw.replace(" ", "");
	                        hindinfo = hindraw.split(',');
	                        hindtaisarv = hindinfo[0];
	                        hindsendid = parseInt(hindinfo[1]);
	                        hind = parseFloat(hindtaisarv + "." +  hindsendid);
	                        
	                        alasummaraw = $(this).parent().parent().parent().find("td:nth-child(5)").html();
	                        alasummaraw = alasummaraw.replace(" ", "");
	                        alasummainfo = alasummaraw.split(',');
	                        alasummataisarv = parseInt(alasummainfo[0]);
	                        alasummasendid = parseFloat(alasummainfo[1]);
	                        alasummavana = parseFloat(alasummataisarv + "." +  alasummasendid);
	                        
	                        loppsummaraw = $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html();
	                        loppsummaraw = loppsummaraw.replace(" ", "");
	                        loppsummainfo = loppsummaraw.split(',');
	                        loppsummataisarv = loppsummainfo[0];
	                        loppsummasendid = parseInt(loppsummainfo[1]);
	                        loppsumma = parseFloat(loppsummataisarv + "." +  loppsummasendid);
	                        
	                        loppsumma = loppsumma - alasummavana;
	                     
	                        alasumma = hind * tk;
	                        
	                        $(this).parent().parent().parent().find("td:nth-child(5)").html(lisatyhikudtuhandikele(alasumma.toFixed(2)) + "€");
	                        lopphind = loppsumma + alasumma;
	                        $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html(lisatyhikudtuhandikele(lopphind.toFixed(2)) + "€");
                            },
		}); 
		
		
		
		
		
		
		$(".eemaldatoode").on({
                 click: function(){ 
	                        tooteid = $(this).attr("data-row");
	                        if ( $( "#tellimuseinfo2" ).length ) {
                               tellimusenr = $("#tellimusekustutaja").attr("data-row");
                               $.ajax({url: "?mode=eemaldatellimusest", data: {id: tellimusenr, id2: tooteid}});
                            } else {
	                         $.ajax({url: "?mode=eemaldakorvist", data: "id=" + tooteid});
	                        }
	                        
	                        alasummaraw = $(this).parent().parent().parent().find("td:nth-child(5)").html();
	                        alasummaraw = alasummaraw.replace(" ", "");
	                        alasummainfo = alasummaraw.split(',');
	                        alasummataisarv = parseInt(alasummainfo[0]);
	                        alasummasendid = parseFloat(alasummainfo[1]);
	                        alasummavana = parseFloat(alasummataisarv + "." +  alasummasendid);
	                        
	                        loppsummaraw = $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html();
	                        loppsummaraw = loppsummaraw.replace(" ", "");
	                        loppsummainfo = loppsummaraw.split(',');
	                        loppsummataisarv = loppsummainfo[0];
	                        loppsummasendid = parseInt(loppsummainfo[1]);
	                        loppsumma = parseFloat(loppsummataisarv + "." +  loppsummasendid);
	                        
	                        loppsumma = loppsumma - alasummavana;
	                        
	                        $(this).parent().parent().parent().parent().parent().find("tr:last-child()").find("td:nth-child(5)").html(lisatyhikudtuhandikele(loppsumma.toFixed(2)) + "€");
	                        $(this).parent().parent().parent().css("display", "none");

	                        peidustabeliridu++;
 
	                        if (tabeliridu - peidustabeliridu == 2){ //korv on tegelikult tyhi nyyd
		                        $(this).parent().parent().parent().parent().css("display", "none");
		                        $(this).parent().parent().parent().parent().parent().parent().children("#korvituhjendaja").css("display", "none");
		                        $(this).parent().parent().parent().parent().parent().parent().parent().children("#kuikorvisonmidagi").css("display", "none");    
	                        }
	                        $(this).parent().parent().parent().parent().parent().parent().parent().children("#paljutooteidkorvis").html(tabeliridu - 2 - peidustabeliridu);
                            },
		 }); 
			
		 $("#korvituhjendaja").on({
                 click: function(){ 
	                        $.ajax({url: "?mode=tuhjendakorv"});
	                        
                            $(this).parent().children("#korvitabel").css("border", "0px");
                            $(this).parent().children("#korvitabel").css("display", "none");
                            
                            $(this).parent().parent().children("#paljutooteidkorvis").html(0);
                            $(this).parent().children("#korvituhjendaja").css("display", "none");
                            $(this).parent().parent().children("#kuikorvisonmidagi").css("display", "none");  

                            },
		 }); 
		 
		 
		 $("#tellimusekustutaja").on({
                 click: function(){ 
	                        tooteid = $(this).attr("data-row");
	                        $.ajax({url: "?mode=kustutatellimus", data: "id=" + tooteid});
	                        
                            $(this).parent().children("#korvitabel").css("border", "0px");
                            $(this).parent().children("#korvitabel").css("display", "none");
                            
                            $(this).parent().parent().children("#paljutooteidkorvis").html(0);
                            $(this).parent().children("#korvituhjendaja").css("display", "none");
                            $(this).parent().parent().children("#kuikorvisonmidagi").css("display", "none");  
                            $(this).parent().parent().children("#tellimuseinfo2").css("display", "none");
                            $("#tellimuseinfo1").css("display", "inline");
                            },
		 }); 
		
		 
		 	      
});

