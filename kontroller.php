<?php 
   include_once('funktsioonid.php');
   alusta_sessioon();
   yhenda_db();
   $mode="";
   
   if(isset($_GET["mode"])){
     $mode=$_GET["mode"];
    };
    
    
      switch($mode){
        case "pea": 
            kuva_pea();
            break;
   
       case "kkk": 
            kuva_kkk();
            break;
 
       case "kontaktid": 
            kuva_kontaktid();
            break;

       case "registreerimine": 
            kuva_registreerimine();
            break;

       case "sisselogimine": 
            kuva_sisselogimine();
            break;
            
       case "toode": 
            kuva_toode();
            break;
 
       case "tooted":
            kuva_tooted();
            break;
          
       case "Otsi":
            kuva_tooted();
            break; 
            
       case "ostukorv":
            kuva_ostukorv();
            break; 
            
       case "valjalogimine":
            kuva_valjalogimine();
            break;
            
       case "lisaostukorvi":
            ajax_lisaostukorvi();
            break;
            
       case "vahandaostukorvist":
            ajax_vahandaostukorvis();
            break;
            
       case "eemaldakorvist":
            ajax_eemaldakorvist();    
            break;
            
       case "kustutatellimus": 
            ajax_kustutatellimus();
            break;
            
       case "lisatellimusse": 
            ajax_lisatellimusse();
            break;
            
       case "lisa tellimusse": 
            header("Location: ?mode=tellimuse haldus&id=" . ajax_lisatellimusse()); 
            exit(0);
            break;
            
       case "vahandatellimusest": 
            ajax_vahandatellimusest();
            break;  
            
       case "eemaldatellimusest": 
            ajax_eemaldatellimusest();
            break;   
            
       case "tuhjendakorv":
            $_SESSION["ostukorv"] = array();
            uuenda_ostukorv();
            break;
            
       case "lisatoode": 
            kuva_lisatoode();
            break;
            
       case "muudatoode": 
            kuva_muudatoode();
            break;
            
       case "muuda toodet": 
            kuva_muudatoode();
            break;
            
       case "tellimuse haldus": 
            kuva_tellimustehaldus();
            break;
            
       case "kasutajatehaldus": 
            kuva_kasutajatehaldus();
            break;
            
       case "muuda kasutaja rolli": 
            kuva_kasutajatehaldus();
            break;
            
       default: 
            kuva_pea();
            break;
      };   

?>