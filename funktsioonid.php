<?php 
    function kuva_pea() {
	    $soovitused = hangi_soovitused();
	    $eripakkumised = hangi_eripakkumised();
	    $arvutikomponendid = hangi_arvutikomponendid_hinnad();
	    $arvutilisaseadmed = hangi_arvutilisaseadmed_hinnad();
	    include('views/head.html');
        include("views/pealeht.html"); 
        include('views/foot.html');
    };
    
    function kuva_kkk() {
	    $kkk = hangi_korkipkys();
	    include('views/head.html');
        include("views/kkk.html"); 
        include('views/foot.html');
    };
    
    function kuva_kontaktid() {
	    $kontaktid = hangi_kontaktid();
	    include('views/head.html');
        include("views/kontaktid.html");  
        include('views/foot.html');
    };
    
    function kuva_registreerimine() {
	    global $connection;
	    $veateated = array();
	    $kasutajanimi = "";
        $parool = "";
	    if(!empty($_POST)){
            if(empty($_POST["kasutajanimi"])){
                array_push($veateated, "Kasutajanimi on sisestamata! <br/>");  
            }
            if(empty($_POST["parool"]) || empty($_POST["parool2"])){       //kas paroolid sisestatud
                array_push($veateated, "Parool on sisestamata! <br/>");    
            }
            if($_POST["parool"] != $_POST["parool"]){                      //kas paroolid samad
                array_push($veateated, "Paroolid on erinevad! <br/>");    
            }
            if(kasutaja_olemas($_POST["kasutajanimi"]) == "olemas") {                             //kas selline kasutaja on juba olemas
                array_push($veateated, "Selline kasutajanimi on juba olemas! <br/>");  
            }
            if(empty($veateated)){
	            loo_ostukorv($_POST["kasutajanimi"]);
	            $kasutajanimi = mysqli_real_escape_string($connection, $_POST["kasutajanimi"]);
                $parool = mysqli_real_escape_string($connection, sha1($_POST["parool"]));
                lisa_kasutaja($parool, $kasutajanimi);
                $_SESSION["logitud"] = true;
                $_SESSION["kasutajanimi"] = $kasutajanimi;
                $_SESSION["teated"] = "Olete edukalt registreeritud ning sisse logitud!";
                header("Location: ?mode=pea");  
                exit(0); 
            }
        }
	    include('views/head.html');
        include("views/registreerimine.html");
        include('views/foot.html');
    };
    
    function kuva_sisselogimine() {
	    global $connection;
	    $veateated = array();
	    $kasutajanimi = "";
        $parool = "";
	    if(!empty($_POST)){
            if(empty($_POST["kasutajanimi"])){
                array_push($veateated, "Kasutajanimi on sisestamata! <br/>");  
            }
            if(empty($_POST["parool"])){
                array_push($veateated, "Parool on sisestamata!");    
            }
            if(empty($veateated)){
	            $kasutajanimi = mysqli_real_escape_string($connection, $_POST["kasutajanimi"]);
                $parool = mysqli_real_escape_string($connection, sha1($_POST["parool"]));
	            if($parool == hangi_parool($kasutajanimi)){
                     $_SESSION["logitud"] = true;
                     $_SESSION["kasutajanimi"] = $kasutajanimi;
                     $_SESSION["teated"] = "Olete edukalt sisselogitud!";
                     header("Location: ?mode=pea");  
                     exit(0); 
                } else {
                     array_push($veateated, "Sisestatud andmed on vigased! Proovi uuesti! <br/>");    
                } 
            }
        }
	    include('views/head.html');
        include("views/sisselogimine.html"); 
        include('views/foot.html');
    };
    
    function kuva_toode() {
	    $arvutikomponendid = hangi_arvutikomponendid_id_d();
	    $arvutilisaseadmed = hangi_arvutilisaseadmed_id_d();
	    $tooteid = 0; 
        $kataloog = 0; 
        if(isset($_GET["id"])){
            $tooteid=$_GET["id"];
		}
	    if(in_array($tooteid, $arvutikomponendid)){ //kas konkreetne toode asub arvutikomponentide kataloogis
		    $kataloog = "arvutikomponendid";
		    $arvutikomponendid = hangi_arvutikomponent($tooteid);				    
        }else if(in_array($tooteid, $arvutilisaseadmed)){
	        $kataloog = "arvutilisaseadmed";
	        $arvutilisaseadmed = hangi_arvutilisaseade($tooteid); 
        }else{  // kui sellist toodet ei leita
            $kataloog = 0;
	        include('views/head.html');
            include("views/toodeteileitud.html");  //erroriga leht
            include('views/foot.html');
            exit(0);
	    }
	    include('views/head.html');
        include("views/tooteleht.html"); 
        include('views/foot.html');
    };
    
    function kuva_tooted() {
	    $arvutikomponendid = hangi_arvutikomponendid_kokkuv6te();
	    $arvutilisaseadmed = hangi_arvutilisaseadmed_kokkuv6te();
	    $tklehelnupud = hangi_perleht();
		$sortbynupud = hangi_sortby();
		
		 if(isset($_GET["sortby"])){
	        $sortby = $_GET["sortby"];
	        if($sortby == "az" || $sortby == "09" || $sortby == "90"){
		        $_SESSION["sortby"] = $sortby;    
	        }   
		} if (isset($_SESSION["sortby"])){
			$sortby = $_SESSION["sortby"];	
		} else {
			$sortby = "az";
		}
		
        if (isset($_GET["pos"]) && !isset($_GET["q"])){
            $posi = $_GET["pos"];
	        $positsioon = explode ( "/", $posi);    
	        if($sortby == "az"){ $tulemused = sorteeri2($positsioon, " ORDER BY toote_nimetus ASC");}
            else if($sortby == "09"){ $tulemused = sorteeri2($positsioon, " ORDER BY hind ASC");}
            else if($sortby == "90"){ $tulemused = sorteeri2($positsioon, " ORDER BY hind DESC");}                     
		} else if (!empty($_GET["q"]) && !isset($_GET["pos"])){
			$q = $_GET["q"];
            $q = strtolower($q); //teeb v2ikesteks t2htedeks k6ik
		    $otsisonad = explode ( " ", $q);
		    if($sortby == "az"){ $tulemused = sorteeri($otsisonad, " ORDER BY toote_nimetus ASC");}
            else if($sortby == "09"){ $tulemused = sorteeri($otsisonad, " ORDER BY hind ASC");}
            else if($sortby == "90"){ $tulemused = sorteeri($otsisonad, " ORDER BY hind DESC");}
            $positsioon = Array("Otsing");	
		} else {
			header("Location: ?mode=pea"); 
            exit(0);
		}
        
        if(isset($_GET["perleht"])){
            $perleht = $_GET["perleht"];
            if($perleht == "20" || $perleht == "40" || $perleht == "60" || $perleht == "80"){
		        $_SESSION["perleht"] = $perleht;    
	        } 
	    } if (isset($_SESSION["perleht"])){
			$perleht = $_SESSION["perleht"];
		} else {
			$perleht = "20"; //default
		}
		  
		
		$leht = 1;
		if(isset($_GET["leht"])){
            $leht=$_GET["leht"];
		    if ($leht < 1 || $leht > (ceil(count($tulemused)/$perleht))) { //kui lehenr on ebaloogiline
			    $leht = 1;
		    }
		}

        include('views/head.html');
        include("views/tooted.html"); 
        include('views/foot.html'); 
    };
    
    
    function kuva_kasutajatehaldus() {
	    global $connection;
	    $tellimus = array();
	    $tellimused = hangi_tellimused_kokkuv6te();
	    $kasutajad = hangi_kasutajad();
        if(!empty($_SESSION["logitud"]) && hangi_oigused($_SESSION["kasutajanimi"]) == "admin"){
	       if (isset($_GET["id"]) && !empty($_GET["id"]) && array_key_exists($_GET["id"], $kasutajad)) { //v6tab optioni stringist esimese integeri,(mis on tellimuse.nr)
	           if (isset($_GET["roll"])) {
		            $id = htmlspecialchars($_GET["id"]);
		            $roll = htmlspecialchars($_GET["roll"]);
		            uuenda_kasutajad($id, $roll);
		            $_SESSION["teated"] = "Kasutaja " . $id . " roll on n��d " . $roll . "!";
		            
		            header("Location: ?mode=pea"); 
                    exit(0);  
	           } else {
	                include('views/head.html');
                    include("views/muudakasutajarolli.html"); 
                    include('views/foot.html');
               }
           } else {
	           
	           include('views/head.html');
               include("views/valikasutaja.html"); 
               include('views/foot.html'); 
           }
        } else {
           $_SESSION["teated"] = "Puuduvad �igused!";
           header("Location: ?mode=pealeht"); 
           exit(0);
        }  
    };
    
    function kuva_ostukorv() {  
	    global $connection; 
	    if (isset($_POST["korvitooted"])) { 
	       $tooted = rtrim($_POST["korvitooted"], ",") . ")";
	       $_SESSION["ostukorv"] = array();
	       lisa_tellimus($tooted);
	       $_SESSION["teated"] = "Tellimus nr." . mysqli_insert_id($connection) . " on edukalt edastatud!";
	       uuenda_ostukorv();
	       header("Location: ?mode=ostukorv"); 
           exit(0);
        } 
	    if(!empty($_SESSION["logitud"])){
	       $supermas = array_merge(hangi_arvutikomponendid_kokkuv6te(), hangi_arvutilisaseadmed_kokkuv6te()); 
	       include('views/head.html');
           include("views/ostukorv.html"); 
           include('views/foot.html'); 
        } else {
           $_SESSION["teated"] = "Ostukorvi n�gemiseks logige sisse!";
           header("Location: ?mode=sisselogimine"); 
           exit(0);
        }        
	};
	
	function ajax_eemaldakorvist() {
	    if (!isset($_SESSION["ostukorv"])) { // kui korvi pole
	             $_SESSION["ostukorv"] = array();
            }
        if (isset($_GET["id"])) {
	        if(array_key_exists($_GET["id"], $_SESSION["ostukorv"] )){
		         unset($_SESSION["ostukorv"][$_GET["id"]]);
	        } 
        }
        uuenda_ostukorv();
        
	};
	
	function ajax_lisaostukorvi() {
	    if (!isset($_SESSION["ostukorv"])) { // kui korvi pole
	        $_SESSION["ostukorv"] = array();
        }
        if (isset($_GET["id"])) {
	        if(array_key_exists($_GET["id"], $_SESSION["ostukorv"] )){
		           $_SESSION["ostukorv"][$_GET["id"]]++;
	        } else {
		           $_SESSION["ostukorv"][$_GET["id"]] = 1;//kui toodet ostukorvis pole siis tuleb see lisada
	        } 
        }
        uuenda_ostukorv();
	};
	
	function ajax_vahandaostukorvis() {
	     if (!isset($_SESSION["ostukorv"])) { // kui korvi pole
	         $_SESSION["ostukorv"] = array();
         }
         if (isset($_GET["id"])) {
	         if(array_key_exists($_GET["id"], $_SESSION["ostukorv"] )){ 
		          if($_SESSION["ostukorv"][$_GET["id"]] > 1){ //kui korvis on ainult 1tk siis enam ei v2hendata
			           $_SESSION["ostukorv"][$_GET["id"]]--;                  
	              }
	         } 
         }
         uuenda_ostukorv();
	};
	
	function ajax_kustutatellimus() { //korras
	     global $connection;
	     if (hangi_oigused($_SESSION["kasutajanimi"]) == "admin" && isset($_GET["id"]) && array_key_exists(reset(array_filter(preg_split("/\D+/", $_GET["id"]))), hangi_tellimused_kokkuv6te())) {
		      $id = mysqli_real_escape_string($connection, $_GET["id"]);
              $query = "DELETE FROM `mmandel_tellimused` WHERE `id`=$id";
              $query = mysqli_real_escape_string($connection, $query);
              mysqli_query($connection, $query);
         }
	};
	
	function ajax_lisatellimusse() { //korras
		global $connection;
        if (hangi_oigused($_SESSION["kasutajanimi"]) == "admin" && isset($_GET["id"]) &&  isset($_GET["id2"]) ) {
	        $id = mysqli_real_escape_string($connection, $_GET["id"]);
	        $id2 = mysqli_real_escape_string($connection, $_GET["id2"]);
	        $tellimus = hangi_tellimus($id);
	        $tooted = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks
        
	        if(array_key_exists($_GET["id2"] , $tooted)) {
	            $tellimus = hangi_tellimus($id);
	            $tooted = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks
	            $uuskogus = $tooted[$id2];
	            $uuskogus++;
	            $asendamis = "'" . $id2 . "'=>'" . $tooted[$id2] . '\'';
	            $asendamillega = "'" . $id2 . "'=>'" . $uuskogus . '\'';
	            $uustooted = str_replace($asendamis, $asendamillega, $tellimus["tooted"]);
	            
	            $query = "UPDATE `mmandel_tellimused` SET `tooted`= \"$uustooted\" WHERE `id`='$id'";
                $andmed = mysqli_query($connection, $query);
                return $id;
            } else  {
	            $tellimus = hangi_tellimus($id);
	            $uustooted = str_replace(")", "", $tellimus["tooted"]);
	            $uustooted = $uustooted . ",'" . $id2 .  "'=>'1')";
	         	       //     $query = mysqli_real_escape_string($connection, $query);
	            $query = "UPDATE `mmandel_tellimused` SET `tooted`= \"$uustooted\" WHERE `id`='$id'";
                mysqli_query($connection, $query);
                return $id;
            }  
        }
        
	};
	function ajax_vahandatellimusest() { //korras
		global $connection;
        if (hangi_oigused($_SESSION["kasutajanimi"]) == "admin" && isset($_GET["id"]) &&  isset($_GET["id2"]) ) {
	        $id = mysqli_real_escape_string($connection, $_GET["id"]);
	        $id2 = mysqli_real_escape_string($connection, $_GET["id2"]);
	        $tellimus = hangi_tellimus($id);
	        $tooted = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks

	        if(array_key_exists($id2 , $tooted)) {
	            $tellimus = hangi_tellimus($id);
	            $tooted = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks
	            $uuskogus = $tooted[$id2];
	            
	            if($uuskogus > 1){ //kui korvis on ainult 1tk siis enam ei v2hendata
	                 $uuskogus--;
	                 $asendamis = "'" . $id2 . "'=>'" . $tooted[$id2] . '\'';
	                 $asendamillega = "'" . $id2 . "'=>'" . $uuskogus . '\'';
	                 $uustooted = str_replace($asendamis, $asendamillega, $tellimus["tooted"]);
	            
	                 $query = "UPDATE `mmandel_tellimused` SET `tooted`= \"$uustooted\" WHERE `id`='$id'";
                     mysqli_query($connection, $query);                 
	            }
            }  
        }  
	};

	
    function ajax_eemaldatellimusest() { //korras
		global $connection;
        if (hangi_oigused($_SESSION["kasutajanimi"]) == "admin" && isset($_GET["id"]) &&  isset($_GET["id2"]) ) {
	        $id = mysqli_real_escape_string($connection, $_GET["id"]);
	        $id2 = mysqli_real_escape_string($connection, $_GET["id2"]);
	        $tellimus = hangi_tellimus($id);
	        $tooted = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks
	        if(array_key_exists($id2 , $tooted)) {
		        $asendamis = "'" . $id2 . "'=>'" . $tooted[$id2] . '\'';
	            $uustooted = str_replace($asendamis, "", $tellimus["tooted"]);
	            $uustooted = str_replace(",,", ",", $uustooted); // kui juhtus kokku 2 koma j2rjest, siis eemalda need

	            $query = "UPDATE `mmandel_tellimused` SET `tooted`= \"$uustooted\" WHERE `id`='$id'";
                mysqli_query($connection, $query); 
            }  
        }  
	};
	
	
function kuva_lisatoode() {
	 global $connection;
	 $toode=array();
	 $toode["toote_id"] = "";
	 $toode["toote_nimetus"] = "";
	 $toode["positsioon1"] = "";
	 $toode["positsioon2"] = "";
	 $toode["positsioon3"] = "";
	 $toode["positsioon4"] = "";
	 $toode["positsioon5"] = "";
	 $toode["positsioon6"] = "";
	 $toode["positsioon7"] = "";
	 $toode["pilt1"] = "";
	 $toode["pilt2"] = "";
	 $toode["pilt3"] = "";
	 $toode["pilt4"] = "";
	 $toode["kirjeldus"] = "";
	 $toode["hind"] = "";
	 $toode["vanahind"] = "";
	            
	 for ($x = 1; $x < 21; $x++) {
	     $toode["tabel" . $x . "a"] = "";
     }
     for ($x = 1; $x < 21; $x++) {
	     $toode["tabel" . $x . "b"] = "";
     }
	 $veateated = array();
	 if(!empty($_POST)){
		    $supermas = array_merge(hangi_arvutikomponendid_id_d(), hangi_arvutilisaseadmed_id_d()); 
            if(empty($_POST["positsioon1"]) || empty($_POST["toote_id"]) || empty($_POST["toote_nimetus"]) || empty($_POST["pilt1"]) || empty($_POST["pilt2"])
               || empty($_POST["pilt3"]) || empty($_POST["pilt4"]) || empty($_POST["hind"]) || empty($_POST["kirjeldus"]) || empty($_POST["tabel1a"]) || in_array($_POST["toote_id"], $supermas)
               || empty($_POST["tabel1b"]) || empty($_POST["tabel2a"]) || empty($_POST["tabel2b"]) || empty($_POST["tabel3a"]) || empty($_POST["tabel3b"]) || !is_numeric($_POST["hind"])){   //kas kohustuslikud v2ljad olemas
	            array_push($veateated, "V�hemalt �ks kohustuslik v�li on t�itmata, v�i pole hind numbriline! <br/>"); 
	            if(in_array($_POST["toote_id"], $supermas)){ //eeeeeeeeeeeeeeeeeeeeeeeei t��ta vb on vaja exists()
                    array_push($veateated, "Sellise toote ID-ga toode on juba olemas!");    
                }
	            foreach($_POST as $id => $vali) {
		            $toode[$id] = $vali;  
	            }
	            include('views/head.html');
                include("views/lisatoode.html"); 
                include('views/foot.html');
                exit(0);
            } else if (empty($veateated)){
	            $toode["toote_id"] = mysqli_real_escape_string($connection, $_POST["toote_id"]);
	            $toode["toote_nimetus"] = mysqli_real_escape_string($connection, $_POST["toote_nimetus"]);
	            $toode["positsioon1"] = mysqli_real_escape_string($connection, $_POST["positsioon1"]);
	            $toode["positsioon2"] = mysqli_real_escape_string($connection, $_POST["positsioon2"]);
	            $toode["positsioon3"] = mysqli_real_escape_string($connection, $_POST["positsioon3"]);
	            $toode["positsioon4"] = mysqli_real_escape_string($connection, $_POST["positsioon4"]);
	            $toode["positsioon5"] = mysqli_real_escape_string($connection, $_POST["positsioon5"]);
	            $toode["positsioon6"] = mysqli_real_escape_string($connection, $_POST["positsioon6"]);
	            $toode["positsioon7"] = mysqli_real_escape_string($connection, $_POST["positsioon7"]);
	            $toode["pilt1"] = mysqli_real_escape_string($connection, $_POST["pilt1"]);
	            $toode["pilt2"] = mysqli_real_escape_string($connection, $_POST["pilt2"]);
	            $toode["pilt3"] = mysqli_real_escape_string($connection, $_POST["pilt3"]);
	            $toode["pilt4"] = mysqli_real_escape_string($connection, $_POST["pilt4"]);
	            $toode["kirjeldus"] = mysqli_real_escape_string($connection, $_POST["kirjeldus"]);
	            $toode["hind"] = mysqli_real_escape_string($connection, $_POST["hind"]);
	            $toode["vanahind"] = mysqli_real_escape_string($connection, $_POST["vanahind"]);
	            
	            for ($x = 1; $x < 21; $x++) {
	            $toode["tabel" . $x . "a"] = mysqli_real_escape_string($connection, $_POST["tabel" . $x . "a"]);
                }
                for ($x = 1; $x < 21; $x++) {
	            $toode["tabel" . $x . "b"] = mysqli_real_escape_string($connection, $_POST["tabel" . $x . "b"]);
                }
	           
                lisa_toode($toode);
                
                $_SESSION["teated"] = "Lisati edukalt toode: " . $toode["toote_id"] . " ! <br/>";
                header("Location: ?mode=pea"); 
                exit(0); 

            }
     } 
	 if(!empty($_SESSION["logitud"]) && hangi_oigused($_SESSION["kasutajanimi"]) == "admin"){
	       include('views/head.html');
           include("views/lisatoode.html"); 
           include('views/foot.html'); 
        } else {
           $_SESSION["teated"] = "Puuduvad �igused!";
           header("Location: ?mode=pealeht"); 
           exit(0);
        }      
};
    
function kuva_muudatoode() {
	    global $connection;
	    $toode=array();
	    $veateated = array();
	    if(!empty($_POST)){
            if(empty($_POST["positsioon1"]) || empty($_POST["toote_id"]) || empty($_POST["toote_nimetus"]) || empty($_POST["pilt1"]) || empty($_POST["pilt2"])
               || empty($_POST["pilt3"]) || empty($_POST["pilt4"]) || empty($_POST["hind"]) || empty($_POST["kirjeldus"]) || empty($_POST["tabel1a"])
               || empty($_POST["tabel1b"]) || empty($_POST["tabel2a"]) || empty($_POST["tabel2b"]) || empty($_POST["tabel3a"]) || empty($_POST["tabel3b"]) || !is_numeric($_POST["hind"])){   //kas kohustuslikud v2ljad olemas
	                array_push($veateated, "V�hemalt �ks kohustuslik v�li on t�itmata, v�i pole hind numbriline! <br/>");  
	                foreach($_POST as $id => $vali) {
		                 $toode[$id] = $vali;  
	                }
	                include('views/head.html');
                    include("views/muudatoode2.html"); 
                    include('views/foot.html');
                    exit(0);
            } else if (empty($veateated)){
	            $toode["toote_id"] = mysqli_real_escape_string($connection, $_POST["toote_id"]);
	            $toode["toote_nimetus"] = mysqli_real_escape_string($connection, $_POST["toote_nimetus"]);
	            $toode["positsioon1"] = mysqli_real_escape_string($connection, $_POST["positsioon1"]);
	            $toode["positsioon2"] = mysqli_real_escape_string($connection, $_POST["positsioon2"]);
	            $toode["positsioon3"] = mysqli_real_escape_string($connection, $_POST["positsioon3"]);
	            $toode["positsioon4"] = mysqli_real_escape_string($connection, $_POST["positsioon4"]);
	            $toode["positsioon5"] = mysqli_real_escape_string($connection, $_POST["positsioon5"]);
	            $toode["positsioon6"] = mysqli_real_escape_string($connection, $_POST["positsioon6"]);
	            $toode["positsioon7"] = mysqli_real_escape_string($connection, $_POST["positsioon7"]);
	            $toode["pilt1"] = mysqli_real_escape_string($connection, $_POST["pilt1"]);
	            $toode["pilt2"] = mysqli_real_escape_string($connection, $_POST["pilt2"]);
	            $toode["pilt3"] = mysqli_real_escape_string($connection, $_POST["pilt3"]);
	            $toode["pilt4"] = mysqli_real_escape_string($connection, $_POST["pilt4"]);
	            $toode["kirjeldus"] = mysqli_real_escape_string($connection, $_POST["kirjeldus"]);
	            $toode["hind"] = mysqli_real_escape_string($connection, $_POST["hind"]);
	            $toode["vanahind"] = mysqli_real_escape_string($connection, $_POST["vanahind"]);
	            
	            for ($x = 1; $x < 21; $x++) {
	            $toode["tabel" . $x . "a"] = mysqli_real_escape_string($connection, $_POST["tabel" . $x . "a"]);
                }
                for ($x = 1; $x < 21; $x++) {
	            $toode["tabel" . $x . "b"] = mysqli_real_escape_string($connection, $_POST["tabel" . $x . "b"]);
                }
                muuda_toode($toode);
                
                $_SESSION["teated"] = "Muudeti edukalt toode: " . $toode["toote_id"] . " ! <br/>";
                header("Location: ?mode=pea"); 
                exit(0); 
            }
        }
	    $supermas = array_merge(hangi_arvutikomponendid_id_d(), hangi_arvutilisaseadmed_id_d()); 
        if(!empty($_SESSION["logitud"]) && hangi_oigused($_SESSION["kasutajanimi"]) == "admin"){
	       if (isset($_GET["id"]) && in_array($_GET["id"], $supermas)) { 
		       $toode = hangi_toode($_GET["id"]); 
	           include('views/head.html');
               include("views/muudatoode2.html"); 
               include('views/foot.html');
           } else {
	           include('views/head.html');
               include("views/muudatoode1.html"); 
               include('views/foot.html'); 
           }
        } else {
           $_SESSION["teated"] = "Puuduvad �igused!";
           header("Location: ?mode=pealeht"); 
           exit(0);
        }   
};

function kuva_tellimustehaldus() {
	    global $connection;
	    $tellimus = array();
	    $tellimused = hangi_tellimused_kokkuv6te();
        if(!empty($_SESSION["logitud"]) && hangi_oigused($_SESSION["kasutajanimi"]) == "admin"){
	       if (isset($_GET["id"]) && !empty($_GET["id"]) && array_key_exists(reset(array_filter(preg_split("/\D+/", $_GET["id"]))), $tellimused)) { //v6tab optioni stringist esimese integeri,(mis on tellimuse.nr)
		       $tellimus = hangi_tellimus(reset(array_filter(preg_split("/\D+/", $_GET["id"])))); 
		       $tellimus["tooted"] = eval("return " . $tellimus["tooted"] . ";");     //stringis kus on kirjeldatud array, tehakse 6igeks arrayks
		       $supermas = array_merge(hangi_arvutikomponendid_kokkuv6te(), hangi_arvutilisaseadmed_kokkuv6te());
		       $supermas2 = array_merge(hangi_arvutikomponendid_id_d(), hangi_arvutilisaseadmed_id_d());  //id-d

	           include('views/head.html');
               include("views/tellimustehaldus2.html"); 
               include('views/foot.html');
           } else {
	           
	           include('views/head.html');
               include("views/tellimustehaldus1.html"); 
               include('views/foot.html'); 
           }
        } else {
           $_SESSION["teated"] = "Puuduvad �igused!";
           header("Location: ?mode=pealeht"); 
           exit(0);
        }  
};


function lisa_tellimus($tooted){
      global $connection;
      $kasutaja = mysqli_real_escape_string($connection, $_SESSION['kasutajanimi']);
      $query = "INSERT INTO `mmandel_tellimused` (kasutaja, tooted, kuupaev) VALUES ('$kasutaja', \"$tooted\", NOW() );";
      mysqli_query($connection, $query);
}

function lisa_kasutaja($parool, $kasutajanimi){
      global $connection;
      $query = "INSERT INTO `mmandel_kasutajad` (kasutaja, parool) VALUES ('$kasutajanimi', '$parool');";
      mysqli_query($connection, $query);
}

function kasutaja_olemas($kasutajanimi){ //korras
      global $connection;
      $kasutaja = mysqli_real_escape_string($connection, $_POST["kasutajanimi"]);
      $query = "SELECT `kasutaja` FROM `mmandel_kasutajad` WHERE `kasutaja`='$kasutaja'";
      $andmed = mysqli_query($connection, $query);
      $rida = "";
      if($rida = mysqli_fetch_row($andmed)){
	     return "olemas"; 
      }
      return "pole";
}


function hangi_tellimused_kokkuv6te(){
  global $connection;
  $tellimused = array();
  $query = "SELECT id, kasutaja, kuupaev FROM mmandel_tellimused";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
	  $tellimused[$rida["id"]]["id"] = $rida["id"];
	  $tellimused[$rida["id"]]["kasutaja"] = $rida["kasutaja"];
	  $tellimused[$rida["id"]]["kuupaev"] = $rida["kuupaev"];
  }
  
  return $tellimused;
}

function hangi_tellimus($id){
  global $connection;
  $tellimus = array();
  $id = mysqli_real_escape_string($connection, $id);
  $query = "SELECT id, kasutaja, tooted, kuupaev FROM mmandel_tellimused WHERE id=$id";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
	  $tellimus["id"] = $rida["id"];
	  $tellimus["kasutaja"] = $rida["kasutaja"];
	  $tellimus["tooted"] = $rida["tooted"];
	  $tellimus["kuupaev"] = $rida["kuupaev"];
  }
  
  return $tellimus;
}

function lisa_toode($toode){
      global $connection;
      $query = "";
      $v6tmed = array("toote_id", "positsioon1", "positsioon2", "positsioon3", "positsioon4", "positsioon5", "positsioon6", "positsioon7", "toote_nimetus", "pilt1", "pilt2", "pilt3", "pilt4", "vanahind", "hind", "kirjeldus", "tabel1a", "tabel2a", "tabel3a", "tabel4a", "tabel5a", "tabel6a", "tabel7a", "tabel8a", "tabel9a", "tabel10a", "tabel11a", "tabel12a", "tabel13a", "tabel14a", "tabel15a", "tabel16a", "tabel17a", "tabel18a", "tabel19a", "tabel20a", "tabel1b", "tabel2b", "tabel3b", "tabel4b", "tabel5b", "tabel6b", "tabel7b", "tabel8b", "tabel9b", "tabel10b", "tabel11b", "tabel12b", "tabel13b", "tabel14b", "tabel15b", "tabel16b", "tabel17b", "tabel18b", "tabel19b", "tabel20b");
      if ($toode["positsioon1"] == "Arvuti komponendid"){
            $query = "INSERT INTO mmandel_arvutikomponendid (toote_id, positsioon1, positsioon2, positsioon3, positsioon4, positsioon5, positsioon6, positsioon7, toote_nimetus, pilt1, pilt2, pilt3, pilt4, vanahind, hind, kirjeldus, tabel1a, tabel2a, tabel3a, tabel4a, tabel5a, tabel6a, tabel7a, tabel8a, tabel9a, tabel10a, tabel11a, tabel12a, tabel13a, tabel14a, tabel15a, tabel16a, tabel17a, tabel18a, tabel19a, tabel20a, tabel1b, tabel2b, tabel3b, tabel4b, tabel5b, tabel6b, tabel7b, tabel8b, tabel9b, tabel10b, tabel11b, tabel12b, tabel13b, tabel14b, tabel15b, tabel16b, tabel17b, tabel18b, tabel19b, tabel20b) VALUES (";
      } else {
            $query = "INSERT INTO mmandel_arvutilisaseadmed (toote_id, positsioon1, positsioon2, positsioon3, positsioon4, positsioon5, positsioon6, positsioon7, toote_nimetus, pilt1, pilt2, pilt3, pilt4, vanahind, hind, kirjeldus, tabel1a, tabel2a, tabel3a, tabel4a, tabel5a, tabel6a, tabel7a, tabel8a, tabel9a, tabel10a, tabel11a, tabel12a, tabel13a, tabel14a, tabel15a, tabel16a, tabel17a, tabel18a, tabel19a, tabel20a, tabel1b, tabel2b, tabel3b, tabel4b, tabel5b, tabel6b, tabel7b, tabel8b, tabel9b, tabel10b, tabel11b, tabel12b, tabel13b, tabel14b, tabel15b, tabel16b, tabel17b, tabel18b, tabel19b, tabel20b) VALUES (";
      }
      
      foreach($v6tmed as $id => $v6ti) {
	        $query = $query . "'" . $toode[$v6ti]. "'" . ",";  
      }

      $query = rtrim($query, ","); //v6tab viimase koma 2ra
      $query = $query . ");";
      $andmed = mysqli_query($connection, $query);
}	

function muuda_toode($toode){ 
      global $connection;
      $query = "";
      $v6tmed = array("toote_id", "positsioon1", "positsioon2", "positsioon3", "positsioon4", "positsioon5", "positsioon6", "positsioon7", "toote_nimetus", "pilt1", "pilt2", "pilt3", "pilt4", "vanahind", "hind", "kirjeldus", "tabel1a", "tabel2a", "tabel3a", "tabel4a", "tabel5a", "tabel6a", "tabel7a", "tabel8a", "tabel9a", "tabel10a", "tabel11a", "tabel12a", "tabel13a", "tabel14a", "tabel15a", "tabel16a", "tabel17a", "tabel18a", "tabel19a", "tabel20a", "tabel1b", "tabel2b", "tabel3b", "tabel4b", "tabel5b", "tabel6b", "tabel7b", "tabel8b", "tabel9b", "tabel10b", "tabel11b", "tabel12b", "tabel13b", "tabel14b", "tabel15b", "tabel16b", "tabel17b", "tabel18b", "tabel19b", "tabel20b");
      if ($toode["positsioon1"] == "Arvuti komponendid"){
            $query = "UPDATE `mmandel_arvutikomponendid` SET ";
      } else {
            $query = "UPDATE `mmandel_arvutilisaseadmed` SET ";
      }
      foreach($v6tmed as $id => $v6ti) {
	        $query = $query . "`" . $v6ti . "`=\"" . $toode[$v6ti] . "\",";  
      }
      //$guery = rtrim($query, ",");
      $query = rtrim($query, ",") . " WHERE `toote_id`='$toode[toote_id]'";

      mysqli_query($connection, $query);
}


function uuenda_ostukorv(){
      global $connection;
      $tooted = "array(";
      foreach($_SESSION["ostukorv"] as $id => $toode){ 
	      $tooted = $tooted . "'" . mysqli_real_escape_string($connection, $id) . "'=>'" . mysqli_real_escape_string($connection, $toode) ."',";
	  }
	  $tooted = $tooted . ")";
	  $tooted = str_replace("(,", "(", $tooted);
	  $tooted = str_replace(",)", ")", $tooted);
	  $kasutaja = mysqli_real_escape_string($connection, $_SESSION["kasutajanimi"]);
      $query = "UPDATE `mmandel_ostukorvid` SET `tooted`=\"$tooted\" WHERE `kasutaja`='$kasutaja'";
      mysqli_query($connection, $query);
}

function hangi_ostukorv($kasutaja){
      global $connection;
      $kasutaja = mysqli_real_escape_string($connection, $kasutaja);
      $query = "SELECT `tooted` FROM `mmandel_ostukorvid` WHERE `kasutaja`='$kasutaja'";
      $andmed = mysqli_query($connection, $query);
      $rida = mysqli_fetch_array($andmed);
	  $tooted1 = $rida["tooted"];
      $tooted = eval("return " . $tooted1 . ";");  
      return $tooted;
}

function loo_ostukorv($kasutaja){
      global $connection;
      $kasutaja = mysqli_real_escape_string($connection, $kasutaja);
	  $query = "INSERT INTO `mmandel_ostukorvid` (tooted, kasutaja) VALUES ('array()', '$kasutaja')"; 
      mysqli_query($connection, $query);
}


function hangi_kasutajad(){
  global $connection;
  $kasutajad = array();
  $query = "SELECT kasutaja, roll FROM mmandel_kasutajad";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
      $ridauus = array();
	  $ridauus["kasutaja"] = $rida["kasutaja"];
	  $ridauus["roll"] = $rida["roll"];
      $kasutajad[$rida["kasutaja"]] = $ridauus;
  }
  
  return $kasutajad;
}

function uuenda_kasutajad($id, $roll){
  global $connection;
  $query = "UPDATE `mmandel_kasutajad` SET `roll`='$roll' WHERE `kasutaja`='$id'";
  $andmed = mysqli_query($connection, $query);
}

function hangi_parool($kasutaja){
  global $connection;
  $parool = "";
  $query = "SELECT `parool` FROM `mmandel_kasutajad` WHERE `kasutaja`='$kasutaja'";
  $andmed = mysqli_query($connection, $query);
  $rida = mysqli_fetch_array($andmed);
  $parool = $rida["parool"];

  
  return $parool;
}

function hangi_oigused($kasutaja){
  global $connection;
  $kasutaja = mysqli_real_escape_string($connection, $kasutaja); 
  $oigus = "";
  $query = "SELECT `roll` FROM `mmandel_kasutajad` WHERE `kasutaja`='$kasutaja'";
  $andmed = mysqli_query($connection, $query);
  $rida = mysqli_fetch_array($andmed);
  $oigus = $rida["roll"];

  
  return $oigus;
}

function hangi_arvutikomponendid_kokkuv6te(){
  global $connection;
  $arvutikomponendid = array();
  $query = "SELECT toote_id, toote_nimetus, pilt1, vanahind, hind, kirjeldus FROM mmandel_arvutikomponendid";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
	  $ridauus = array();
	  $ridauus["toote_id"] = $rida["toote_id"];
	  $ridauus["toote_nimetus"] = $rida["toote_nimetus"];
	  $ridauus["pilt1"] = $rida["pilt1"];
	  $ridauus["vanahind"] = $rida["vanahind"];
	  $ridauus["hind"] = $rida["hind"];
	  $ridauus["kirjeldus"] = $rida["kirjeldus"];
      $arvutikomponendid[$rida["toote_id"]] = $ridauus;
  }
  
  return $arvutikomponendid;
}	
function hangi_arvutilisaseadmed_kokkuv6te(){
  global $connection;
  $arvutilisaseadmed = array();
  $query = "SELECT toote_id, toote_nimetus, pilt1, vanahind, hind, kirjeldus FROM mmandel_arvutilisaseadmed";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
      $ridauus = array();
	  $ridauus["toote_id"] = $rida["toote_id"];
	  $ridauus["toote_nimetus"] = $rida["toote_nimetus"];
	  $ridauus["pilt1"] = $rida["pilt1"];
	  $ridauus["vanahind"] = $rida["vanahind"];
	  $ridauus["hind"] = $rida["hind"];
	  $ridauus["kirjeldus"] = $rida["kirjeldus"];
      $arvutilisaseadmed[$rida["toote_id"]] = $ridauus;
  }
  
  return $arvutilisaseadmed;
}

function hangi_sortby(){
  global $connection;
  $sortbynupud = array();
  $query = "SELECT * FROM mmandel_sortby";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($sortbynupud, $rida);
  }
  
  return $sortbynupud;
}	
function hangi_perleht(){
  global $connection;
  $perleht = array();
  $query = "SELECT * FROM mmandel_perleht";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($perleht, $rida["tklehel"]);
  }
  
  return $perleht;
}


function hangi_toode($tooteid){
  global $connection;
  $tooteid = mysqli_real_escape_string($connection, $tooteid); 
  $arvutikomponent = array();
  $query = "SELECT * FROM `mmandel_arvutikomponendid` WHERE `toote_id`= '$tooteid'";
  $andmed = mysqli_query($connection, $query);
  $rida = mysqli_fetch_array($andmed);
  
  if (empty($rida)) {
	  $query = "SELECT * FROM `mmandel_arvutilisaseadmed` WHERE `toote_id`= '$tooteid'";
      $andmed = mysqli_query($connection, $query);
      $rida = mysqli_fetch_array($andmed);
  }
  
  return $rida;
}

	
function hangi_arvutikomponent($tooteid){
  global $connection;
  $arvutikomponent = array();
  $tooteid = mysqli_real_escape_string($connection, $tooteid); 
  $query = "SELECT * FROM `mmandel_arvutikomponendid` WHERE `toote_id`= '$tooteid'";
  $andmed = mysqli_query($connection, $query);
  
  $rida = mysqli_fetch_array($andmed);
  
  return $rida;
}

	


function hangi_arvutilisaseade($tooteid){
  global $connection;
  $arvutilisaseade = array();
  $tooteid = mysqli_real_escape_string($connection, $tooteid); 
  $query = "SELECT * FROM `mmandel_arvutilisaseadmed` WHERE `toote_id`= '$tooteid'";
  $andmed = mysqli_query($connection, $query);
  
  $rida = mysqli_fetch_array($andmed);
  
  return $rida;
}

function hangi_arvutikomponendid_id_d(){
  global $connection;
  $arvutikomponendid = array();
  $query = "SELECT toote_id FROM mmandel_arvutikomponendid";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($arvutikomponendid, $rida["toote_id"]);
  }
  
  return $arvutikomponendid;
}		

function hangi_arvutilisaseadmed_id_d(){
  global $connection;
  $arvutilisaseadmed = array();
  $query = "SELECT toote_id FROM mmandel_arvutilisaseadmed";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($arvutilisaseadmed, $rida["toote_id"]);
  }
  
  return $arvutilisaseadmed;
}	

function hangi_arvutikomponendid_hinnad(){
  global $connection;
  $arvutikomponendid = array();
  $query = "SELECT toote_id, vanahind, hind FROM mmandel_arvutikomponendid";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
      $arvutikomponendid[$rida["toote_id"]] = $rida;
  }
  
  return $arvutikomponendid;
}

function hangi_arvutilisaseadmed_hinnad(){
  global $connection;
  $arvutilisaseadmed = array();
  $query = "SELECT toote_id, vanahind, hind FROM mmandel_arvutilisaseadmed";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
     $arvutilisaseadmed[$rida["toote_id"]] = $rida;
  }
  
  return $arvutilisaseadmed;
}

	
function hangi_soovitused(){
  global $connection;
  $soovitused = array();
  $query = "SELECT * FROM mmandel_soovitused";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
	  $soovitused[$rida["toote_id"]] = $rida;
  }
  
  return $soovitused;
}

function hangi_eripakkumised(){
  global $connection;
  $eripakkumised = array();
  $query = "SELECT * FROM mmandel_eripakkumised";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
	  $eripakkumised[$rida["toote_id"]] = $rida;
  }
  
  return $eripakkumised;
}	
	
function hangi_korkipkys(){
  global $connection;
  $kkk = array();
  $query = "SELECT * FROM mmandel_korkipkys";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($kkk, $rida);
  }
  
  return $kkk;
}

function hangi_kontaktid(){
  global $connection;
  $kontaktid = array();
  $query = "SELECT * FROM mmandel_kontaktid";
  $andmed = mysqli_query($connection, $query);
  
  while($rida = mysqli_fetch_array($andmed)){
    array_push($kontaktid, $rida);
  }
  
  return $kontaktid;
}
	
	
function yhenda_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="t3st3r123";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa mootoriga �hendust");
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}
		
	
function alusta_sessioon(){
        session_set_cookie_params(60*60);
	    session_start();
}
	
function kuva_valjalogimine(){
        lopeta_sessioon();
        header("Location: ?mode=pea"); 
        exit(0);
}
function lopeta_sessioon(){
	    $_SESSION = array();
	    if (isset($_COOKIE[session_name()])) {
 	        setcookie(session_name(), '', time()-42000, '/');
	    }
	    session_destroy();
}	
	
	

function tooteidotsing2($otsisonad) {
	    global $connection;
	    $tulemused = array(); 
       
	    if(1 == count($otsisonad)){ //kas on ainult yks s6na, sest tooteid-d on ainult 1 s6nalised 
            $query = "SELECT toote_id  FROM mmandel_arvutilisaseadmed WHERE toote_id='mysqli_real_escape_string($connection, $otsisonad[0])'";
            $andmed = mysqli_query($connection, $query);
            $rida = mysqli_fetch_array($andmed);
            if(!empty($rida["toote_id"])){
	            array_push($tulemused, $rida["toote_id"]);
            }
            
            $query = "SELECT toote_id FROM mmandel_arvutikomponendid WHERE toote_id='mysqli_real_escape_string($connection, $otsisonad[0])'";
            $andmed = mysqli_query($connection, $query);
            $rida = mysqli_fetch_array($andmed);
            if(!empty($rida["toote_id"])){
	            array_push($tulemused, $rida["toote_id"]);
            }
        }		                    
     
        return $tulemused;     
}; //funktsiooni l6pp 
	


function sorteeri($otsisonad, $kuidassorteerida) { //otsing, ei ole case-sensitive
    global $connection;
	$tulemused = array(); //l6pptulemused
    for ($i = 0; $i < 2; $i++ ) { // 2 kataloogi kust otsida
	    if($i==0){
		    $query = "SELECT toote_id FROM mmandel_arvutikomponendid WHERE ";  //valitakse algstring
	    }else{
		    $query = "SELECT toote_id FROM mmandel_arvutilisaseadmed WHERE ";  
	    } 
	    
        for ($y = 0; $y < count($otsisonad); $y++ ) { //paneb kokku db p2ringu k2su
            $sona = mysqli_real_escape_string($connection, $otsisonad[$y]);
            $query = $query . " LOCATE('{$sona}',`toote_nimetus`) > 0 || LOCATE('{$sona}',`kirjeldus`) > 0 || LOCATE('{$sona}',`toote_id`) > 0 ";
            if($y != count($otsisonad)-1){
	            $query = $query . " && ";   
            }
        }      
        $query = $query . mysqli_real_escape_string($connection, $kuidassorteerida);
        $andmed = mysqli_query($connection, $query);
	    while($rida = mysqli_fetch_array($andmed)){
            array_push($tulemused, $rida["toote_id"]);
        }
    }
	return $tulemused;
	
}; //funktsiooni l6pp 


function sorteeri2($positsioon, $kuidassorteerida) { //k6ik konkreetses tooteharus olevad tooteid-d pannakse tulemuste massiivi 
	 global $connection;
     $tulemused = array();
	  
	 if($positsioon[0] == "Arvuti komponendid"){
		$kataloog = "arvutikomponendid"; 
	 } else if($positsioon[0] == "Arvuti lisaseadmed") {
		$kataloog = "arvutilisaseadmed"; 
	 } else {
		return $tulemused;
	 } 
	   
     $query = "SELECT toote_id FROM mmandel_$kataloog WHERE ";
     for ($y = 0; $y < count($positsioon); $y++ ) { //paneb kokku db p2ringu k2su
        $query = $query . "positsioon" . ($y+1) . " = '" . mysqli_real_escape_string($connection, $positsioon[$y]) . "'";
        if($y != count($positsioon)-1){
	        $query = $query . " && ";   
        }
     } 

     $query = $query . mysqli_real_escape_string($connection, $kuidassorteerida);
     $andmed = mysqli_query($connection, $query);
  
     while($rida = mysqli_fetch_array($andmed)){
        array_push($tulemused, $rida["toote_id"]);
     }
     
     return $tulemused;
}; //funktsiooni l6pp 